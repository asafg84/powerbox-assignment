// import TodoService from "@/services/TodoService" this would be used to talk with server-side
import _ from 'lodash'

const state = {
    todoItems: [{
        action: `Get up`,
        status: false,
        _id: `123`
    }, {
        action: `Survive`,
        status: false,
        _id: `234`
    }, {
        action: `Get back to bed`,
        status: false,
        _id: `345`
    }]
}

const mutations = {
    MUTATIONS_UPDATE_TODO_STATUS: (state, payload) => {
        const _id = payload._id
        const status = payload.status
        const index = _.findIndex(state.todoItems, {
            _id
        })
        state.todoItems[index].status = status
    },
    MUTATIONS_UPDATE_TODO_ACTION: (state, payload) => {
        const _id = payload._id
        const action = payload.action
        const index = _.findIndex(state.todoItems, {
            _id
        })
        state.todoItems[index].action = action
    },
    MUTATIONS_REMOVE_TODO_BY_ID: (state, payload) => {
        const _id = payload
        const index = _.findIndex(state.todoItems, {
            _id
        })
        state.todoItems.splice(index, 1)
    }
}

const actions = {
    ACTIONS_UPDATE_TODO_STATUS: ({
        commit
    }, payload) => {
        return new Promise(async (resolve, reject) => {
            try {
                //   const result = await TodoService.updateTodoStatus(payload)
                commit('MUTATIONS_UPDATE_TODO_STATUS', payload)
                resolve(true)
            } catch (e) {
                reject(e)
            }
        })
    },
    ACTIONS_UPDATE_TODO_ACTION: ({
        commit
    }, payload) => {
        return new Promise(async (resolve, reject) => {
            try {
                //   const result = await TodoService.updateTodoText(payload)
                commit('MUTATIONS_UPDATE_TODO_ACTION', payload)
                resolve(true)
            } catch (e) {
                reject(e)
            }
        })
    },
    ACTIONS_DELETE_TODO: async ({
        commit
    }, payload) => {
        return new Promise(async (resolve, reject) => {
            try {
                //   const result = await TodoService.deleteTodo(payload)
                commit('MUTATIONS_REMOVE_TODO_BY_ID', payload)
                resolve(true)
            } catch (e) {
                reject(e)
            }
        })
    },
}

const getters = {
    GETTERS_TODO_ITEMS: state => {
        return state.todoItems
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}