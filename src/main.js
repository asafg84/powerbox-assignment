import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import {
  library
} from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret, faTrash, faPencilAlt
} from '@fortawesome/free-solid-svg-icons'
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

library.add(faTrash)
library.add(faPencilAlt)
Vue.component('font-awesome-icon', FontAwesomeIcon)

import VueConfetti from 'vue-confetti'
Vue.use(VueConfetti)

import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')