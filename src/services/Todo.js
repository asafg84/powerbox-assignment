import Api from '@/services/Api'

export default {
    getTodos() {
        return Api().get(`todos`)
    },
    updateTodoStatus(todo) {
        return Api().put(`todos/${ todo._id }`, todo)
    }
}